using System.Linq;
using Castle.Core.Internal;
using KCore.Common.Data;
using KCore.Common.Extensions;
using Shouldly;
using Xunit;

namespace K.Approval.Api.Test.Common
{
    public class StringExtensionTest
    {
        [Fact]
        public void StringCompareTest()
        {
           "source".Compare("Source").ShouldBe(true);
           "SOURCE".Compare("SouRce").ShouldBe(true);
           "source".Compare("Source  ").ShouldBe(false);
           "source".Compare(" Source").ShouldBe(false);
        }
    }
}
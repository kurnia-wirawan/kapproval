using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using FakeItEasy;
using K.Approval.Api.Common;
using K.Approval.Api.DataContext;
using K.Approval.Api.Features.SubmitProcess;
using K.Approval.Api.Test.Builders;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Shouldly;
using Xunit;

namespace K.Approval.Api.Test.Features
{
    public class SubmitProcessTest
    {
        private readonly WorkflowDataContext _workflowDataContext;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly Validation _validation;
        private readonly Handler _handler;
        private Api.Features.GetInbox.Validation _validationGetInbox;
        private Api.Features.GetInbox.Handler _handlerGetInbox;
        private Api.Features.Approval.Validation _validationApproval;
        private Api.Features.Approval.Handler _handlerApproval;
        
        public SubmitProcessTest()
        {
            var contextOptions = new DbContextOptionsBuilder<WorkflowDataContext>().UseInMemoryDatabase("TestPortalDatabase").Options;
            _workflowDataContext = new WorkflowDataContext(contextOptions);
            _httpContextAccessor = A.Fake<IHttpContextAccessor>();
            var ipAddress = IPAddress.Parse("127.0.0.1");
            A.CallTo(() => _httpContextAccessor.HttpContext.Connection.RemoteIpAddress).Returns(ipAddress);
            A.CallTo(() => _httpContextAccessor.HttpContext.User.Identity.IsAuthenticated).Returns(true);
            A.CallTo(() => _httpContextAccessor.HttpContext.Request.Path).Returns(new PathString(@"/fakes/FAKE/FAKE/fake/"));
            A.CallTo(() => _httpContextAccessor.HttpContext.Request.PathBase).Returns(new PathString(@""));
            A.CallTo(() => _httpContextAccessor.HttpContext.Request.QueryString).Returns(new QueryString(@""));
            A.CallTo(() => _httpContextAccessor.HttpContext.Request.Host).Returns(new HostString(@"fakeapi:6008"));
            _validation = new Validation(_workflowDataContext);
            _handler = new Handler(_httpContextAccessor, _workflowDataContext);
        }

        [Fact]
        public async Task RequestNullShouldBeFailed()
        {
            var result = await _validation.InternalValidate(null);
            result.HttpStatusCode.ShouldBe(460);
            result.ErrorCode.ShouldBe("603");
            result.ErrorDescription.ShouldBe("Kesalahan, Request NULL");
        }
        
        [Fact]
        public async Task RequestWithInvalidCompanyShouldBeFailed()
        {
            await _workflowDataContext.Database.EnsureDeletedAsync();
            var company = new CompanyBuilder().build();
            var applicationRegistration = new ApplicationRegistrationBuilder()
                .WithAuthKey("FAKE")
                .WithCompany(company.Id)
                .build();
            await _workflowDataContext.Companies.AddAsync(company);
            await _workflowDataContext.ApplicationRegistrations.AddAsync(applicationRegistration);
            await _workflowDataContext.SaveChangesAsync();
            
            var request = new Request
            {
                AuthKey = "TEST KEY"
            };
            
            var result = await _validation.InternalValidate(request);
            result.HttpStatusCode.ShouldBe(460);
            result.ErrorCode.ShouldBe("600");
            result.ErrorDescription.ShouldBe("Kesalahan, Aplikasi Key sudah tidak aktif");
        }
        
        [Fact]
        public async Task RequestWithInvalidAuthKeyShouldBeFailed()
        {
            await _workflowDataContext.Database.EnsureDeletedAsync();
            var company = new CompanyBuilder().WithRowStatus(1).build();
            var applicationRegistration = new ApplicationRegistrationBuilder()
                .WithAuthKey("FAKE1")
                .WithCompany(company.Id)
                .build();
            await _workflowDataContext.Companies.AddAsync(company);
            await _workflowDataContext.ApplicationRegistrations.AddAsync(applicationRegistration);
            await _workflowDataContext.SaveChangesAsync();
            
            var request = new Request
            {
                AuthKey = "FAKE"
            };
            
            var result = await _validation.InternalValidate(request);
            result.HttpStatusCode.ShouldBe(460);
            result.ErrorCode.ShouldBe("600");
            result.ErrorDescription.ShouldBe("Kesalahan, Aplikasi Key sudah tidak aktif");
        }
        
        [Fact]
        public async Task RequestWithInvalidActionShouldBeFailed()
        {
            await _workflowDataContext.Database.EnsureDeletedAsync();
            var company = new CompanyBuilder().WithRowStatus(0).build();
            var applicationRegistration = new ApplicationRegistrationBuilder()
                .WithAuthKey("FAKE")
                .WithCompany(company.Id)
                .build();
            await _workflowDataContext.Companies.AddAsync(company);
            await _workflowDataContext.ApplicationRegistrations.AddAsync(applicationRegistration);
            await _workflowDataContext.SaveChangesAsync();
            
            var request = new Request
            {
                AuthKey = "FAKE",
                Action = "FAKE"
            };
            
            var result = await _validation.InternalValidate(request);
            result.HttpStatusCode.ShouldBe(460);
            result.ErrorCode.ShouldBe("601");
            result.ErrorDescription.ShouldBe("Kesalahan, Request Action tidak valid");
        }

        [Fact]
        public async Task HappyFlowShouldBeSuccess()
        {
            
            await _workflowDataContext.Database.EnsureDeletedAsync();
            
            var company = new CompanyBuilder().WithRowStatus(0).build();
            var workflow = new WorkflowBuilder().build();
           
            var applicationRegistration = new ApplicationRegistrationBuilder()
                .WithAuthKey("FAKE")
                .WithCompany(company.Id)
                .build();
            
            var userProfile = new UserProfileBuilder()
                .WithCompany(company.Id)
                .WithEmail("fake@email.com")
                .WithEmployeeCode("USER-SUBMITER")
                .build();
            
            var userViewer = new UserProfileBuilder()
                .WithCompany(company.Id)
                .WithEmail("viewer@email.com")
                .WithEmployeeCode("USER-VIEWER")
                .build();
            
            var userApprove = new UserProfileBuilder()
                .WithCompany(company.Id)
                .WithEmail("approver@email.com")
                .WithEmployeeCode("USER-APPROVER")
                .build();
            
            var tProcessRequest = new TProcessRequestBuilder()
                .WithCompany(company.CompanyCode)
                .WithWorkflow(workflow.Id)
                .WithModuleId("FAKE MODULE")
                .WithEffectiveDate(DateTime.Now.AddDays(-5))
                .build();

            var processActivity1 = new TProcessActivityBuilder()
                .WithActivityIndex(1)
                .WithNewStatus("NEW", "NEW")
                .WithProcessRequest(tProcessRequest.Id)
                .WithPostSubject("PA1 Anda Telah Mengajukan Dokumen [DocumentNumber]")
                .WithSubjectName("PA1 Dokumen No [DocumentNumber] Baru Diajukan")
                .WithViewSubject("PA1 Dokumen No [DocumentNumber] Baru Diajukan")
                .build();

            var processActivityActors1 = new List<TProcessActivityActor>
            {
                new TProcessActivityActorBuilder()
                    .WithProcessActivity(processActivity1.Id)
                    .WithActionType((byte)InboxActionType.View)
                    .WithActor(userViewer.EmployeeCode, userViewer.FullName)
                    .build(),
                new TProcessActivityActorBuilder()
                    .WithProcessActivity(processActivity1.Id)
                    .WithActionType((byte)InboxActionType.Approval)
                    .WithActor(userApprove.EmployeeCode, userApprove.FullName)
                    .build(),
                new TProcessActivityActorBuilder()
                    .WithProcessActivity(processActivity1.Id)
                    .WithActionType((byte)InboxActionType.Initiator)
                    .WithActor(userProfile.EmployeeCode, userProfile.FullName)
                    .build()
            };
            
            var processActivity2 = new TProcessActivityBuilder()
                .WithActivityIndex(2)
                .WithNewStatus("APPROVED", "APPROVED")
                .WithProcessRequest(tProcessRequest.Id)
                .WithPostSubject("PA2 Dokumen [DocumentNumber] menunggu persetujuan")
                .WithSubjectName("PA2 Dokumen [DocumentNumber] membutuhkan persetujuan dari anda")
                .WithViewSubject("PA2 View Dokumen [DocumentNumber]")
                .build();
            
            var processActivityActors2 = new List<TProcessActivityActor>
            {
                new TProcessActivityActorBuilder()
                    .WithProcessActivity(processActivity2.Id)
                    .WithActionType((byte)InboxActionType.View)
                    .WithActor(userProfile.EmployeeCode, userProfile.FullName)
                    .build(),
                new TProcessActivityActorBuilder()
                    .WithProcessActivity(processActivity2.Id)
                    .WithActionType((byte)InboxActionType.View)
                    .WithActor(userViewer.EmployeeCode, userViewer.FullName)
                    .build(),
                new TProcessActivityActorBuilder()
                    .WithProcessActivity(processActivity2.Id)
                    .WithActionType((byte)InboxActionType.Approval)
                    .WithActor(userApprove.EmployeeCode, userApprove.FullName)
                    .build()
            };
            
            var processActivity3 = new TProcessActivityBuilder()
                .WithActivityIndex(3)
                .WithNewStatus("CLOSED", "CLOSED")
                .WithProcessRequest(tProcessRequest.Id)
                .WithPostSubject("PA3 POST [DocumentNumber] CLOSED")
                .WithSubjectName("PA3 SUBJECT [DocumentNumber] CLOSED")
                .WithViewSubject("PA3 VIEW [DocumentNumber] CLOSED")
                .build();
            
            
            var documentNumber = new DocumentNumberBuilder().WithDocumentCode("REQUEST_NUMBER").build();
            
            await _workflowDataContext.Companies.AddAsync(company);
            await _workflowDataContext.Workflows.AddAsync(workflow);
            await _workflowDataContext.ApplicationRegistrations.AddAsync(applicationRegistration);
            await _workflowDataContext.UserProfiles.AddRangeAsync(userProfile, userViewer, userApprove);
            await _workflowDataContext.TProcessRequests.AddAsync(tProcessRequest);
            await _workflowDataContext.TProcessActivities.AddRangeAsync(processActivity1, processActivity2, processActivity3);
            await _workflowDataContext.TProcessActivityActors.AddRangeAsync(processActivityActors1.ToArray());
            await _workflowDataContext.TProcessActivityActors.AddRangeAsync(processActivityActors2.ToArray());
            await _workflowDataContext.DocumentNumbers.AddAsync(documentNumber);
            await _workflowDataContext.DocumentNumberPatterns.AddAsync(new DocumentNumberPatternBuilder()
                .WithDocumentCode(documentNumber.Id)
                .WithSeparator("")
                .WithLastIndex(13)
                .WithFixedValue("DOC")
                .WithLengthNumber(7)
                .build());
            await _workflowDataContext.SaveChangesAsync();
            
            var request = new Request
            {
                AuthKey = "FAKE",
                Action = "submit",
                Email = "fake@email.com",
                ModuleId = "FAKE MODULE",
                Description = "FAKE",
                ObjectKey = Guid.NewGuid().ToString(),
                RequestDate = DateTime.Now,
                DocumentName = "DOC FAKE",
                DocumentNumber = "DOC3033"
            };
            
            var validation = await _validation.InternalValidate(request);
            validation.HttpStatusCode.ShouldBe(200);

            var handler = await _handler.Handle(request, CancellationToken.None);
            handler.HttpStatusCode.ShouldBe(200);

            var documentNumberLast = await _workflowDataContext.DocumentNumbers
                .Include("DocumentNumberPatterns")
                .FirstOrDefaultAsync(x => x.DocumentCode == "REQUEST_NUMBER");
            documentNumberLast.ShouldNotBeNull();
            var documentNumberPattern = documentNumberLast.DocumentNumberPatterns.FirstOrDefault();
            documentNumberPattern.ShouldNotBeNull();
            documentNumberPattern.LastIndex.ShouldBe(14);

            var ff = await _workflowDataContext.ProcessRequests
                .Include("ProcessActivities") //ProcessActivityActors
                .FirstOrDefaultAsync();
            ff.RequestNumber.ShouldBe("0000014");

            var gg = await _workflowDataContext.RequestStatuses.FirstOrDefaultAsync(x => x.RequestNumber == ff.RequestNumber);
            gg.ShouldNotBeNull();
            gg.DocumentName.ShouldBe(request.DocumentName);
            gg.ObjectKey.ShouldBe(request.ObjectKey);
            gg.DocumentNumber.ShouldBe(request.DocumentNumber);
            gg.ActorCode.ShouldBe(userApprove.EmployeeCode);
            gg.Subject.ShouldBe($"PA2 View Dokumen {request.DocumentNumber}");
            gg.IsComplete.ShouldBe((byte)0);
            gg.NewRequestStatus.ShouldBe(processActivity2.NewStatus);
            
            _validationGetInbox = new Api.Features.GetInbox.Validation(_workflowDataContext);
            _handlerGetInbox = new Api.Features.GetInbox.Handler(_httpContextAccessor, _workflowDataContext);
            var requestInbox = new Api.Features.GetInbox.Request
            {
                Email = userViewer.Email,
                AuthKey = "FAKE"
            };
            
            
            var q1 = await _validationGetInbox.InternalValidate(requestInbox);
            q1.HttpStatusCode.ShouldBe((int)HttpStatusCode.OK);

            var q2 = await _handlerGetInbox.Handle(requestInbox,CancellationToken.None);
            q2.Value.Count.ShouldBeGreaterThan(0);

            var d1 = q2.Value.Data.FirstOrDefault(x => x.RequestStatus == "NEW");
            d1.ShouldNotBeNull();
            d1.Subject.ShouldBe($"PA1 Dokumen No {request.DocumentNumber} Baru Diajukan");
            d1.IsComplete.ShouldBe(true);
            d1.ActorCodeAssignee.ShouldBe(userViewer.EmployeeCode);
            
            var d2 = q2.Value.Data.FirstOrDefault(x => x.RequestStatus == "APPROVED");
            d2.ShouldNotBeNull();
            d2.Subject.ShouldBe($"PA2 View Dokumen {request.DocumentNumber}");
            d2.IsComplete.ShouldBe(false);
            d2.ActorCodeAssignee.ShouldBe(userViewer.EmployeeCode);
            
            
            var requestInbox3 = new Api.Features.GetInbox.Request
            {
                Email = userProfile.Email,
                AuthKey = "FAKE"
            };
            
            var q5 = await _validationGetInbox.InternalValidate(requestInbox3);
            q5.HttpStatusCode.ShouldBe((int)HttpStatusCode.OK);
            
            var q6 = await _handlerGetInbox.Handle(requestInbox3,CancellationToken.None);
            
            d1 = q6.Value.Data.FirstOrDefault(x => x.RequestStatus == "NEW");
            d1.ShouldNotBeNull();
            d1.Subject.ShouldBe($"PA1 Anda Telah Mengajukan Dokumen {request.DocumentNumber}");
            d1.IsComplete.ShouldBe(true);
            d1.ActorCodeAssignee.ShouldBe(userProfile.EmployeeCode);
            
            d2 = q6.Value.Data.FirstOrDefault(x => x.RequestStatus == "APPROVED");
            d2.ShouldNotBeNull();
            d2.Subject.ShouldBe($"PA2 Dokumen {request.DocumentNumber} menunggu persetujuan");
            d2.IsComplete.ShouldBe(false);
            d2.ActorCodeAssignee.ShouldBe(userProfile.EmployeeCode);
            
            var requestInbox2 = new Api.Features.GetInbox.Request
            {
                Email = userApprove.Email,
                AuthKey = "FAKE"
            };
            
            var q3 = await _validationGetInbox.InternalValidate(requestInbox2);
            q3.HttpStatusCode.ShouldBe((int)HttpStatusCode.OK);
            
            var q4 = await _handlerGetInbox.Handle(requestInbox2,CancellationToken.None);
            q4.Value.Count.ShouldBeGreaterThan(0);
            d1 = q4.Value.Data.FirstOrDefault(x => x.RequestStatus == "NEW");
            d1.ShouldNotBeNull();
            d1.Subject.ShouldBe($"PA1 Dokumen No {request.DocumentNumber} Baru Diajukan");
            d1.IsComplete.ShouldBe(true);
            d1.ActorCodeAssignee.ShouldBe(userApprove.EmployeeCode);
            
            d2 = q4.Value.Data.FirstOrDefault(x => x.RequestStatus == "APPROVED");
            d2.ShouldNotBeNull();
            d2.Subject.ShouldBe($"PA2 Dokumen {request.DocumentNumber} membutuhkan persetujuan dari anda");
            d2.IsComplete.ShouldBe(false);
            d2.ActorCodeAssignee.ShouldBe(userApprove.EmployeeCode);
            
            var request2 = new Approval.Api.Features.Approval.Request
            {
                Id = d2.Id,
                Description = "FAKE DESC",
                AuthKey = "FAKE",
                ObjectId = d2.ObjectId,
                RequestNumber = d2.RequestNumber,
                Action = "approve",
                Email = userApprove.Email
            };
            
            _validationApproval = new Api.Features.Approval.Validation(_workflowDataContext);
            var v3 = await _validationApproval.InternalValidate(request2);
            v3.HttpStatusCode.ShouldBe((int)HttpStatusCode.OK);
            
            _handlerApproval = new Api.Features.Approval.Handler(_httpContextAccessor, _workflowDataContext);
            var h2 = await _handlerApproval.Handle(request2, CancellationToken.None);
            h2.HttpStatusCode.ShouldBe((int)HttpStatusCode.OK);
            h2.Value.NewStatus.ToLower().ShouldBe(RequestStatusEnum.Closed.ToString().ToLower());

            var requestStatus = await _workflowDataContext.RequestStatuses.FirstOrDefaultAsync(x =>
                x.RequestNumber == request2.RequestNumber);
            requestStatus.ShouldNotBeNull();
            requestStatus.NewRequestStatus.ToLower().ShouldBe(RequestStatusEnum.Closed.ToString().ToLower());
            requestStatus.ActorCode.ShouldBe(userApprove.EmployeeCode);
            requestStatus.ObjectKey.ShouldBe(request2.ObjectId);
            requestStatus.DocumentName.ShouldBe(request.DocumentName);
            requestStatus.DocumentNumber.ShouldBe(request.DocumentNumber);
            requestStatus.ActorName.ShouldBe(userApprove.FullName);
            requestStatus.CompleteDate.HasValue.ShouldBe(true);
            requestStatus.CompleteDate.Value.Date.ShouldBe(DateTime.Now.Date);
            requestStatus.IsComplete.ShouldBe((byte)1);
            requestStatus.Subject.ShouldBe($"PA3 VIEW {request.DocumentNumber} CLOSED");
            requestStatus.RequestDate.ShouldBe(request.RequestDate);
            requestStatus.LastAssignTo.ShouldBe(userApprove.FullName);
            requestStatus.LastAssignDate.Date.ShouldBe(DateTime.Now.Date);
            requestStatus.CommitmentDate.ShouldBeNull();

            var requestActivities = await _workflowDataContext.RequestActivities.Where(x =>
                x.RequestNumber == request2.RequestNumber).OrderByDescending(x => x.CreatedDate).ToListAsync();
            requestActivities.Count.ShouldBe(3);
            requestActivities[0].ActorCode.ShouldBe(userApprove.EmployeeCode);
            requestActivities[0].DocumentName.ShouldBe(request.DocumentName);
            requestActivities[0].DocumentNumber.ShouldBe(request.DocumentNumber);
            requestActivities[0].ObjectKey.ShouldBe(request2.ObjectId);
            requestActivities[0].RequestNumber.ShouldBe(request2.RequestNumber);
            requestActivities[0].ActivityIndex.ShouldBe(processActivity3.ActivityIndex);
            requestActivities[0].ActorCode.ShouldBe(userApprove.EmployeeCode);
            requestActivities[0].ActorName.ShouldBe(userApprove.FullName);
            requestActivities[0].RequestStatus.ShouldBe(processActivity3.NewStatus);
            requestActivities[0].DisplayStatus.ShouldBe(processActivity3.DisplayName);
            requestActivities[0].SubjectName.ShouldBe($"PA3 VIEW {request.DocumentNumber} CLOSED");
            requestActivities[0].IsComplete.ShouldBe((byte)1);
            requestActivities[0].ActionName.ShouldBe(RequestAction.Closed.ToString());
            requestActivities[0].ActionDate.HasValue.ShouldBe(true);
            requestActivities[0].ActionDate.Value.Date.ShouldBe(DateTime.Now.Date);
            
            requestActivities[1].ActorCode.ShouldBe(userApprove.EmployeeCode);
            requestActivities[1].DocumentName.ShouldBe(request.DocumentName);
            requestActivities[1].DocumentNumber.ShouldBe(request.DocumentNumber);
            requestActivities[1].ObjectKey.ShouldBe(request2.ObjectId);
            requestActivities[1].RequestNumber.ShouldBe(request2.RequestNumber);
            requestActivities[1].ActivityIndex.ShouldBe(processActivity2.ActivityIndex);
            requestActivities[1].ActorCode.ShouldBe(userApprove.EmployeeCode);
            requestActivities[1].ActorName.ShouldBe(userApprove.FullName);
            requestActivities[1].RequestStatus.ShouldBe(processActivity2.NewStatus);
            requestActivities[1].DisplayStatus.ShouldBe(processActivity2.DisplayName);
            requestActivities[1].SubjectName.ShouldBe($"PA2 Dokumen {request.DocumentNumber} membutuhkan persetujuan dari anda");
            requestActivities[1].IsComplete.ShouldBe((byte)1);
            0.ShouldBe(string.Compare(RequestAction.Approve.ToString(), requestActivities[1].ActionName, StringComparison.InvariantCultureIgnoreCase));
            requestActivities[1].ActionDate.HasValue.ShouldBe(true);
            requestActivities[1].ActionDate.Value.Date.ShouldBe(DateTime.Now.Date);
            
            
            requestActivities[2].ActorCode.ShouldBe(userProfile.EmployeeCode);
            requestActivities[2].DocumentName.ShouldBe(request.DocumentName);
            requestActivities[2].DocumentNumber.ShouldBe(request.DocumentNumber);
            requestActivities[2].ObjectKey.ShouldBe(request.ObjectKey);
            requestActivities[2].RequestNumber.ShouldBe(request2.RequestNumber);
            requestActivities[2].ActivityIndex.ShouldBe(processActivity1.ActivityIndex);
            requestActivities[2].ActorCode.ShouldBe(userProfile.EmployeeCode);
            requestActivities[2].ActorName.ShouldBe(userProfile.FullName);
            requestActivities[2].RequestStatus.ShouldBe(processActivity1.NewStatus);
            requestActivities[2].DisplayStatus.ShouldBe(processActivity1.DisplayName);
            requestActivities[2].SubjectName.ShouldBe($"PA1 Dokumen No {request.DocumentNumber} Baru Diajukan");
            requestActivities[2].IsComplete.ShouldBe((byte)1);
            0.ShouldBe(string.Compare(processActivity1.NewStatus, requestActivities[2].ActionName, StringComparison.InvariantCultureIgnoreCase));
            requestActivities[2].ActionDate.HasValue.ShouldBe(true);
            requestActivities[1].ActionDate.Value.Date.ShouldBe(DateTime.Now.Date);
        }
        
        [Fact]
        public async Task MultiApproverShouldBeSuccess()
        {
            
            await _workflowDataContext.Database.EnsureDeletedAsync();
            
            var company = new CompanyBuilder().WithRowStatus(0).build();
            var workflow = new WorkflowBuilder().build();
           
            var applicationRegistration = new ApplicationRegistrationBuilder()
                .WithAuthKey("FAKE")
                .WithCompany(company.Id)
                .build();
            
            var userProfile = new UserProfileBuilder()
                .WithCompany(company.Id)
                .WithEmail("fake@email.com")
                .WithEmployeeCode("USER-SUBMITER")
                .build();
            
            var userViewer = new UserProfileBuilder()
                .WithCompany(company.Id)
                .WithEmail("viewer@email.com")
                .WithEmployeeCode("USER-VIEWER")
                .build();
            
            var userApprove1 = new UserProfileBuilder()
                .WithCompany(company.Id)
                .WithEmail("approver@email.com")
                .WithEmployeeCode("USER-APPROVER")
                .build();
            
            var userApprove2 = new UserProfileBuilder()
                .WithCompany(company.Id)
                .WithEmail("approver2@email.com")
                .WithEmployeeCode("USER-APPROVER-II")
                .build();
            
            var tProcessRequest = new TProcessRequestBuilder()
                .WithCompany(company.CompanyCode)
                .WithWorkflow(workflow.Id)
                .WithModuleId("FAKE MODULE")
                .WithEffectiveDate(DateTime.Now.AddDays(-5))
                .build();

            var processActivity1 = new TProcessActivityBuilder()
                .WithActivityIndex(1)
                .WithNewStatus("NEW", "NEW")
                .WithProcessRequest(tProcessRequest.Id)
                .WithPostSubject("PA1 Anda Telah Mengajukan Dokumen [DocumentNumber]")
                .WithSubjectName("PA1 Dokumen No [DocumentNumber] Baru Diajukan")
                .WithViewSubject("PA1 Dokumen No [DocumentNumber] Baru Diajukan")
                .build();

            var processActivityActors1 = new List<TProcessActivityActor>
            {
                new TProcessActivityActorBuilder()
                    .WithProcessActivity(processActivity1.Id)
                    .WithActionType((byte)InboxActionType.View)
                    .WithActor(userViewer.EmployeeCode, userViewer.FullName)
                    .build(),
                new TProcessActivityActorBuilder()
                    .WithProcessActivity(processActivity1.Id)
                    .WithActionType((byte)InboxActionType.View)
                    .WithActor(userApprove1.EmployeeCode, userApprove1.FullName)
                    .WithActionType(1)
                    .build(),
                new TProcessActivityActorBuilder()
                    .WithProcessActivity(processActivity1.Id)
                    .WithActionType((byte)InboxActionType.View)
                    .WithActor(userApprove2.EmployeeCode, userApprove2.FullName)
                    .WithActionType(2)
                    .build(),
                new TProcessActivityActorBuilder()
                    .WithProcessActivity(processActivity1.Id)
                    .WithActionType((byte)InboxActionType.Initiator)
                    .WithActor(userProfile.EmployeeCode, userProfile.FullName)
                    .build()
            };
            
            var processActivity2 = new TProcessActivityBuilder()
                .WithActivityIndex(2)
                .WithNewStatus("APPROVED", "APPROVED")
                .WithProcessRequest(tProcessRequest.Id)
                .WithPostSubject("PA2 Dokumen [DocumentNumber] menunggu persetujuan")
                .WithSubjectName("PA2 Dokumen [DocumentNumber] membutuhkan persetujuan dari anda")
                .WithViewSubject("PA2 View Dokumen [DocumentNumber]")
                .build();
            
            var processActivityActors2 = new List<TProcessActivityActor>
            {
                new TProcessActivityActorBuilder()
                    .WithProcessActivity(processActivity2.Id)
                    .WithActionType((byte)InboxActionType.View)
                    .WithActor(userProfile.EmployeeCode, userProfile.FullName)
                    .build(),
                new TProcessActivityActorBuilder()
                    .WithProcessActivity(processActivity2.Id)
                    .WithActionType((byte)InboxActionType.View)
                    .WithActor(userViewer.EmployeeCode, userViewer.FullName)
                    .build(),
                new TProcessActivityActorBuilder()
                    .WithProcessActivity(processActivity2.Id)
                    .WithActionType((byte)InboxActionType.Approval)
                    .WithActor(userApprove1.EmployeeCode, userApprove1.FullName)
                    .build(),
                new TProcessActivityActorBuilder()
                    .WithProcessActivity(processActivity2.Id)
                    .WithActionType((byte)InboxActionType.Approval)
                    .WithActor(userApprove2.EmployeeCode, userApprove2.FullName)
                    .build(),
            };
            
            var processActivity3 = new TProcessActivityBuilder()
                .WithActivityIndex(3)
                .WithNewStatus("CLOSED", "CLOSED")
                .WithProcessRequest(tProcessRequest.Id)
                .WithPostSubject("PA3 POST [DocumentNumber] CLOSED")
                .WithSubjectName("PA3 SUBJECT [DocumentNumber] CLOSED")
                .WithViewSubject("PA3 VIEW [DocumentNumber] CLOSED")
                .build();
            
            
            var documentNumber = new DocumentNumberBuilder().WithDocumentCode("REQUEST_NUMBER").build();
            
            await _workflowDataContext.Companies.AddAsync(company);
            await _workflowDataContext.Workflows.AddAsync(workflow);
            await _workflowDataContext.ApplicationRegistrations.AddAsync(applicationRegistration);
            await _workflowDataContext.UserProfiles.AddRangeAsync(userProfile, userViewer, userApprove1, userApprove2);
            await _workflowDataContext.TProcessRequests.AddAsync(tProcessRequest);
            await _workflowDataContext.TProcessActivities.AddRangeAsync(processActivity1, processActivity2, processActivity3);
            await _workflowDataContext.TProcessActivityActors.AddRangeAsync(processActivityActors1.ToArray());
            await _workflowDataContext.TProcessActivityActors.AddRangeAsync(processActivityActors2.ToArray());
            await _workflowDataContext.DocumentNumbers.AddAsync(documentNumber);
            await _workflowDataContext.DocumentNumberPatterns.AddAsync(new DocumentNumberPatternBuilder()
                .WithDocumentCode(documentNumber.Id)
                .WithSeparator("")
                .WithLastIndex(13)
                .WithFixedValue("DOC")
                .WithLengthNumber(7)
                .build());
            await _workflowDataContext.SaveChangesAsync();
            
            var request = new Request
            {
                AuthKey = "FAKE",
                Action = "submit",
                Email = "fake@email.com",
                ModuleId = "FAKE MODULE",
                Description = "FAKE",
                ObjectKey = Guid.NewGuid().ToString(),
                RequestDate = DateTime.Now,
                DocumentName = "DOC FAKE",
                DocumentNumber = "DOC3033"
            };
            
            var validation = await _validation.InternalValidate(request);
            validation.HttpStatusCode.ShouldBe(200);

            var handler = await _handler.Handle(request, CancellationToken.None);
            handler.HttpStatusCode.ShouldBe(200);

            var documentNumberLast = await _workflowDataContext.DocumentNumbers
                .Include("DocumentNumberPatterns")
                .FirstOrDefaultAsync(x => x.DocumentCode == "REQUEST_NUMBER");
            documentNumberLast.ShouldNotBeNull();
            var documentNumberPattern = documentNumberLast.DocumentNumberPatterns.FirstOrDefault();
            documentNumberPattern.ShouldNotBeNull();
            documentNumberPattern.LastIndex.ShouldBe(14);

            var ff = await _workflowDataContext.ProcessRequests
                .Include("ProcessActivities") //ProcessActivityActors
                .FirstOrDefaultAsync();
            ff.RequestNumber.ShouldBe("0000014");

            var gg = await _workflowDataContext.RequestStatuses.FirstOrDefaultAsync(x => x.RequestNumber == ff.RequestNumber);
            gg.ShouldNotBeNull();
            gg.DocumentName.ShouldBe(request.DocumentName);
            gg.ObjectKey.ShouldBe(request.ObjectKey);
            gg.DocumentNumber.ShouldBe(request.DocumentNumber);
            gg.ActorCode.ShouldBe(userApprove1.EmployeeCode);
            gg.Subject.ShouldBe($"PA2 View Dokumen {request.DocumentNumber}");
            gg.IsComplete.ShouldBe((byte)0);
            gg.NewRequestStatus.ShouldBe(processActivity2.NewStatus);

            var fkd = await _workflowDataContext.RequestActivities.Where(x => x.RequestNumber == ff.RequestNumber && x.RowStatus == 0).ToListAsync();
            

        }
    }
}
using System;
using K.Approval.Api.DataContext;
using KCore.Common.Test;

namespace K.Approval.Api.Test.Builders
{
    public class CompanyBuilder : BaseFluentBuilder<Company>
    {
        public CompanyBuilder()
        {
            SetProperty(x => x.Id, Guid.NewGuid());
            SetProperty(x => x.RowStatus, RowStatus);
        }
        
        public CompanyBuilder WithRowStatus(byte data)
        {
            SetProperty(x => x.RowStatus, data);
            return this;
        }
    }
}
using System;
using K.Approval.Api.DataContext;
using KCore.Common.Test;

namespace K.Approval.Api.Test.Builders
{ 
    public class DocumentNumberBuilder : BaseFluentBuilder<DocumentNumber>
    {
        public DocumentNumberBuilder()
        {
            SetProperty(x => x.Id, Guid.NewGuid());
            SetProperty(x => x.RowStatus, RowStatus);
        }
        
        public DocumentNumberBuilder WithDocumentCode(string data)
        {
            SetProperty(x => x.DocumentCode, data);
            return this;
        }
        
    }
}
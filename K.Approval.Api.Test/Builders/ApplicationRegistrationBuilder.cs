using System;
using K.Approval.Api.DataContext;
using KCore.Common.Test;

namespace K.Approval.Api.Test.Builders
{
    public class ApplicationRegistrationBuilder : BaseFluentBuilder<ApplicationRegistration>
    {
        public ApplicationRegistrationBuilder()
        {
            SetProperty(x => x.Id, Guid.NewGuid());
            SetProperty(x => x.RowStatus, RowStatus);
        }

        public ApplicationRegistrationBuilder WithAuthKey(string data)
        {
            SetProperty(x => x.ApplicationSecret, data);
            return this;
        }
        
        public ApplicationRegistrationBuilder WithCompany(Guid data)
        {
            SetProperty(x => x.CompanyId, data);
            return this;
        }

        
    }
}
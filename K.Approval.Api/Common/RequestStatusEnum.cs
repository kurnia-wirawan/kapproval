namespace K.Approval.Api.Common
{
    public enum RequestStatusEnum
    {
        New,
        Approved,
        Closed,
        Rejected,
        Revised
    }
}
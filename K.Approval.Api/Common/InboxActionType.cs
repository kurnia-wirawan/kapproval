namespace K.Approval.Api.Common
{
    public enum InboxActionType
    {
        View = 1,
        Approval = 2,
        Initiator = 3
    }
}
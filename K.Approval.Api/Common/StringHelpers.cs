using System;
using System.Net;
using KCore.Common.Fault;

namespace K.Approval.Api.Common
{
    public static class StringHelpers
    {
        public static string CreateSubject(this string value, string requestNumber, string username, string actionName, string documentNumber)
        {
            return value.Replace("[TicketNumber]", requestNumber)
                .Replace("[UserName]", username)
                .Replace("[ActionName]", actionName)
                .Replace("[DocumentNumber]", documentNumber);
        }

        public static RequestAction ToRequestAction(this string value)
        {
            switch (value.ToLower())
            {
                case  "closed":
                    return RequestAction.Closed;
                case  "submit":
                    return RequestAction.Submit;
                case  "reject":
                    return RequestAction.Reject;
                case  "revise":
                    return RequestAction.Revise;
                case  "approve":
                    return RequestAction.Approve;
                case  "postpone":
                    return RequestAction.Postpone;
                case  "resubmit":
                    return RequestAction.ReSubmit;
                default:
                    throw new ApiException("Kesalahan Action Request", HttpStatusCode.BadRequest);
            }
        }

        public static bool Compare(this string src, RequestStatusEnum status)
        {
            return string.Equals(src, status.ToString(), StringComparison.InvariantCultureIgnoreCase);
        }

        /*public static string CreateSubject(this string value, string requestNumber, string employeeCode, string requestAction, string documentNumber)
        {
            //var val = RequestActionToString.ToString(requestAction);
            return value.Replace("[TicketNumber]", requestNumber)
                .Replace("[UserName]", employeeCode)
                .Replace("[ActionName]", requestAction)
                .Replace("[DocumentNumber]", documentNumber);
        }*/
    }
}
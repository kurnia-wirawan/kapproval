using K.Approval.Api.DataContext;

namespace K.Approval.Api.Common
{
    public class BaseRequest
    {
        public string AuthKey { get; set; }
        internal UserProfile UserProfile { get; set; }
        internal Company Company { get; set; }

        internal string GetAccessor()
        {
            if (UserProfile == null) return "DEFAULT";
            if (!string.IsNullOrEmpty(UserProfile.Email))
            {
                return UserProfile.Email.Length < 24 ? UserProfile.Email : UserProfile.Email.Substring(0, 24);
            }

            if (!string.IsNullOrEmpty(UserProfile.FullName))
            {
                return UserProfile.FullName.Length < 24 ? UserProfile.FullName : UserProfile.FullName.Substring(0, 24);
            }
            return "DEFAULT";
        }
    }
}
using System;

namespace K.Approval.Api.Common
{
    public class RequestParameter
    {
        public RequestAction ActionName { get; set; }
        public string ObjectKey { get; set; }
        public DateTime RequestDate { get; set; }
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string DocumentName { get; set; }
        public string DocumentNumber { get; set; }
        public string RequestNumber { get; set; }
        public string Description { get; set; }
        public string ModuleId { get; set; }
        public string CompanyId { get; set; }
    }
}
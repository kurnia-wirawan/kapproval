using System;

namespace K.Approval.Api.Common
{
    public enum RequestAction
    {
        Submit,
        Approve,
        Revise,
        Reject,
        Postpone,
        Closed,
        ReSubmit
    }

    public static class RequestActionExtension
    {
        public static string ToName(this RequestAction request)
        {
            switch (request)
            {
                case RequestAction.Submit: return "Submit";
                case RequestAction.Approve: return "Approve";
                case RequestAction.Revise: return "Revise";
                case RequestAction.Reject: return "Reject";
                case RequestAction.Postpone: return "Postpone";
                case RequestAction.Closed: return "Closed";
                case RequestAction.ReSubmit: return "ReSubmit";
                default:
                    throw new ArgumentOutOfRangeException(nameof(request), request, null);
            }
        }
    }

    /*class RequestActionToString
    {
        public static string ToString(RequestAction action)
        {
            switch (action)
            {
                case RequestAction.Submit:
                    return "Pengajuan";
                case RequestAction.Approve:
                    return "Disetujui";
                case RequestAction.Revise:
                    return "Koreksi";
                case RequestAction.Reject:
                    return "Ditolak";
                case RequestAction.Postpone:
                    return "Ditahan";
                case RequestAction.Closed:
                    return "Ditutup";
                case RequestAction.ReSubmit:
                    return "Pengajuan Ulang";
                default:
                    return "";
            }
        }
    }*/
}
﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace K.Approval.Api.Migrations
{
    public partial class ProcessActivityActorTemplaye : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ActivityIndex",
                table: "TProcessActivity",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "ApprovalJavascriptAction",
                table: "TProcessActivity",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CreatedBy",
                table: "TProcessActivity",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedDate",
                table: "TProcessActivity",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DisplayName",
                table: "TProcessActivity",
                maxLength: 64,
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "EndValue",
                table: "TProcessActivity",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ModifiedBy",
                table: "TProcessActivity",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedDate",
                table: "TProcessActivity",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NewStatus",
                table: "TProcessActivity",
                maxLength: 64,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Other01JavascriptAction",
                table: "TProcessActivity",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Other02JavascriptAction",
                table: "TProcessActivity",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Other03JavascriptAction",
                table: "TProcessActivity",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Other04JavascriptAction",
                table: "TProcessActivity",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PostSubject",
                table: "TProcessActivity",
                maxLength: 128,
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ProcessRequestId",
                table: "TProcessActivity",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<int>(
                name: "SlaTime",
                table: "TProcessActivity",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SlaType",
                table: "TProcessActivity",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "StartValue",
                table: "TProcessActivity",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SubjectName",
                table: "TProcessActivity",
                maxLength: 128,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UrlAction",
                table: "TProcessActivity",
                nullable: true);

            migrationBuilder.AddColumn<byte>(
                name: "UrlActionType",
                table: "TProcessActivity",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ViewJavascriptAction",
                table: "TProcessActivity",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ViewSubject",
                table: "TProcessActivity",
                maxLength: 128,
                nullable: true);

            migrationBuilder.CreateTable(
                name: "TProcessActivityActors",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RowStatus = table.Column<byte>(nullable: false),
                    ProcessRequestId = table.Column<Guid>(nullable: false),
                    TProcessActivityId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TProcessActivityActors", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TProcessActivityActors_TProcessActivity_TProcessActivityId",
                        column: x => x.TProcessActivityId,
                        principalTable: "TProcessActivity",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TProcessActivityActors_TProcessActivityId",
                table: "TProcessActivityActors",
                column: "TProcessActivityId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TProcessActivityActors");

            migrationBuilder.DropColumn(
                name: "ActivityIndex",
                table: "TProcessActivity");

            migrationBuilder.DropColumn(
                name: "ApprovalJavascriptAction",
                table: "TProcessActivity");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "TProcessActivity");

            migrationBuilder.DropColumn(
                name: "CreatedDate",
                table: "TProcessActivity");

            migrationBuilder.DropColumn(
                name: "DisplayName",
                table: "TProcessActivity");

            migrationBuilder.DropColumn(
                name: "EndValue",
                table: "TProcessActivity");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "TProcessActivity");

            migrationBuilder.DropColumn(
                name: "ModifiedDate",
                table: "TProcessActivity");

            migrationBuilder.DropColumn(
                name: "NewStatus",
                table: "TProcessActivity");

            migrationBuilder.DropColumn(
                name: "Other01JavascriptAction",
                table: "TProcessActivity");

            migrationBuilder.DropColumn(
                name: "Other02JavascriptAction",
                table: "TProcessActivity");

            migrationBuilder.DropColumn(
                name: "Other03JavascriptAction",
                table: "TProcessActivity");

            migrationBuilder.DropColumn(
                name: "Other04JavascriptAction",
                table: "TProcessActivity");

            migrationBuilder.DropColumn(
                name: "PostSubject",
                table: "TProcessActivity");

            migrationBuilder.DropColumn(
                name: "ProcessRequestId",
                table: "TProcessActivity");

            migrationBuilder.DropColumn(
                name: "SlaTime",
                table: "TProcessActivity");

            migrationBuilder.DropColumn(
                name: "SlaType",
                table: "TProcessActivity");

            migrationBuilder.DropColumn(
                name: "StartValue",
                table: "TProcessActivity");

            migrationBuilder.DropColumn(
                name: "SubjectName",
                table: "TProcessActivity");

            migrationBuilder.DropColumn(
                name: "UrlAction",
                table: "TProcessActivity");

            migrationBuilder.DropColumn(
                name: "UrlActionType",
                table: "TProcessActivity");

            migrationBuilder.DropColumn(
                name: "ViewJavascriptAction",
                table: "TProcessActivity");

            migrationBuilder.DropColumn(
                name: "ViewSubject",
                table: "TProcessActivity");
        }
    }
}

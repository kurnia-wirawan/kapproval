﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace K.Approval.Api.Migrations
{
    public partial class update101 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DeviceId",
                table: "UserProfiles");

            migrationBuilder.DropColumn(
                name: "UserName",
                table: "UserProfiles");

            migrationBuilder.AddColumn<string>(
                name: "RequestNumber",
                table: "ProcessRequests",
                maxLength: 24,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<int>(
                name: "YearNumber",
                table: "DocumentNumberLogs",
                nullable: false,
                oldClrType: typeof(short));

            migrationBuilder.AlterColumn<int>(
                name: "MonthNumber",
                table: "DocumentNumberLogs",
                nullable: false,
                oldClrType: typeof(byte));

            migrationBuilder.CreateTable(
                name: "ApplicationRegistrations",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RowStatus = table.Column<byte>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: false),
                    CompanyId = table.Column<Guid>(nullable: false),
                    ApplicationKey = table.Column<string>(maxLength: 24, nullable: false),
                    ApplicationSecret = table.Column<string>(maxLength: 24, nullable: false),
                    NodeCluster = table.Column<string>(maxLength: 24, nullable: true),
                    Owner = table.Column<string>(maxLength: 128, nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 24, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 24, nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApplicationRegistrations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ApplicationRegistrations_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EmailTasks",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RowStatus = table.Column<byte>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: false),
                    TaskFrom = table.Column<string>(maxLength: 64, nullable: false),
                    SourceId = table.Column<Guid>(nullable: false),
                    EmailFrom = table.Column<string>(maxLength: 128, nullable: false),
                    EmailTo = table.Column<string>(maxLength: 128, nullable: false),
                    EmailCc = table.Column<string>(maxLength: 1024, nullable: true),
                    EmailSubject = table.Column<string>(maxLength: 128, nullable: false),
                    EmailBody = table.Column<string>(nullable: false),
                    ResendCount = table.Column<int>(nullable: false),
                    ErrorMessage = table.Column<string>(maxLength: 128, nullable: true),
                    StartSend = table.Column<DateTime>(nullable: true),
                    EndSend = table.Column<DateTime>(nullable: true),
                    IsSuccess = table.Column<bool>(nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 24, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 24, nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmailTasks", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ApplicationRegistrations_CompanyId",
                table: "ApplicationRegistrations",
                column: "CompanyId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ApplicationRegistrations");

            migrationBuilder.DropTable(
                name: "EmailTasks");

            migrationBuilder.DropColumn(
                name: "RequestNumber",
                table: "ProcessRequests");

            migrationBuilder.AddColumn<string>(
                name: "DeviceId",
                table: "UserProfiles",
                maxLength: 32,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserName",
                table: "UserProfiles",
                maxLength: 24,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<short>(
                name: "YearNumber",
                table: "DocumentNumberLogs",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<byte>(
                name: "MonthNumber",
                table: "DocumentNumberLogs",
                nullable: false,
                oldClrType: typeof(int));
        }
    }
}

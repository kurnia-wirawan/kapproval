using System;
using System.Threading;
using System.Threading.Tasks;
using K.Approval.Api.DataContext;
using MediatR;

namespace K.Approval.Api.Infrastructures
{
    public class TransactionDecorator <TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
    {
        private readonly WorkflowDataContext _dataContext;

        public TransactionDecorator(WorkflowDataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            using (var transaction = await _dataContext.Database.BeginTransactionAsync(cancellationToken))
            {
                try
                {
                    var response = await next();
                    await _dataContext.SaveChangesAsync(cancellationToken);
                    transaction.Commit();
                    return response;
                }
                catch (Exception exception)
                {
                    transaction.Rollback();
                    throw exception;
                }
            }
        }
    }
}
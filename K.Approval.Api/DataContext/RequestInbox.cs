using System;
using System.ComponentModel.DataAnnotations;

namespace K.Approval.Api.DataContext
{
    public class RequestInbox
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public byte RowStatus { get; set; }
        [Required]
        public byte[] RowVersion { get; set; }
        [Required]
        public Guid ProcessActivityId { get; set; }
        [Required]
        [StringLength(64)]
        public string ObjectId { get; set; }
        [StringLength(32)]
        [Required]
        public string RequestNumber { get; set; }
        [StringLength(64)]
        [Required]
        public string DocumentName { get; set; }
        [StringLength(32)]
        [Required]
        public string DocumentNumber { get; set; }
        [StringLength(24)]
        [Required]
        public string ActorCodeRequester { get; set; }
        [StringLength(64)]
        [Required]
        public string ActorNameRequester { get; set; }
        public DateTime AssignDate { get; set; }
        public DateTime RequestDate { get; set; }
        [StringLength(128)]
        [Required]
        public string Subject { get; set; }
        [StringLength(512)]
        public string Description { get; set; }
        public DateTime? CommitmentDate { get; set; }
        [StringLength(24)]
        public string ActorCodeAssignee { get; set; }
        [StringLength(64)]
        public string ActorNameAssignee { get; set; }
        public bool IsComplete { get; set; }
        [StringLength(1024)]
        [Required]
        public string ActivityUrl { get; set; }
        [StringLength(64)]
        [Required]
        public string RequestStatus { get; set; }
        [StringLength(64)]
        [Required]
        public string DisplayStatus { get; set; }
        public DateTime? CompleteDate { get; set; }
        public bool IsDelegatee { get; set; }
        [StringLength(24)]
        public string RequestDelegateFromId { get; set; }
        [StringLength(64)]
        public string RequestDelegateFromName { get; set; }
        public bool HasView { get; set; }
        public DateTime? ViewDate { get; set; }
        [StringLength(512)]
        public string ViewNetworkInfo { get; set; }
        public byte UrlActionType { get; set; }
        [StringLength(512)]
        [Required]
        public string UrlAction { get; set; }
        [StringLength(256)]
        [Required]
        public string JavascriptAction { get; set; }
        [StringLength(32)]
        [Required]
        public string ActionType { get; set; }
        [StringLength(24)]
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        [StringLength(24)]
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    
        public virtual ProcessActivity ProcessActivity { get; set; }
    }
}
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace K.Approval.Api.DataContext
{
    public class TProcessRequest
    {
        public TProcessRequest()
        {
            TProcessActivities = new HashSet<TProcessActivity>();
        }
        [Key]
        public Guid Id { get; set; }
        [Required]
        public byte RowStatus { get; set; }
        [Required]
        public byte[] RowVersion { get; set; }
        [Required]
        [StringLength(24)]
        public string CompanyId { get; set; }
        [Required]
        [StringLength(24)]
        public string ModuleId { get; set; }
        [Required]
        [StringLength(128)]
        public string ModuleName { get; set; }
        [Required]
        [StringLength(128)]
        public string DisplayProcessName { get; set; }
        [Required]
        public DateTime EffectiveDate { get; set; }
        [Required]
        public Guid WorkflowId { get; set; }
        [StringLength(1000)]
        public string Notes { get; set; }
        [Required]
        [StringLength(24)]
        public string CreatedBy { get; set; }
        [Required]
        public DateTime CreatedDate { get; set; }
        [StringLength(24)]
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public virtual ICollection<TProcessActivity> TProcessActivities { get; set; }
        public virtual Workflow Workflow { get; set; }
    }
}
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace K.Approval.Api.DataContext
{
    public class UserProfile
    {
        public UserProfile()
        {
            UserLogs = new HashSet<UserLog>();
        }
    
        [Key]
        public Guid Id { get; set; }
        [Required]
        public byte RowStatus { get; set; }
        [Required]
        public byte[] RowVersion { get; set; }
        [Required]
        public Guid CompanyId { get; set; }
        [Required]
        [StringLength(24)]
        public string EmployeeCode { get; set; }
        [Required]
        [StringLength(64)]
        public string FullName { get; set; }
        [Required]
        [StringLength(128)]
        public string Email { get; set; }
        [Required]
        [StringLength(24)]
        public string CreatedBy { get; set; }
        [Required]
        public DateTime CreatedDate { get; set; }
        [StringLength(24)]
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    
        public virtual Company Company { get; set; }
        public virtual UserImage UserImage { get; set; }
        public virtual ICollection<UserLog> UserLogs { get; set; }
        public virtual UserSecurity UserSecurity { get; set; }

    }
}
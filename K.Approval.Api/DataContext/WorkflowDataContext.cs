using System;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.EntityFrameworkCore;

namespace K.Approval.Api.DataContext
{
    public class WorkflowDataContext : DbContext
    {
        
        public WorkflowDataContext(DbContextOptions options) : base(options)
        {
        }
        
        public virtual DbSet<EmailTask> EmailTasks { get; set; }
        public virtual DbSet<ApplicationRegistration> ApplicationRegistrations { get; set; }
        public virtual DbSet<Company> Companies { get; set; }
        public virtual DbSet<UserLog> UserLogs { get; set; }
        public virtual DbSet<UserImage> UserImages { get; set; }
        public virtual DbSet<UserProfile> UserProfiles { get; set; }
        public virtual DbSet<UserSecurity> UserSecurities { get; set; }
        public virtual DbSet<DocumentNumber> DocumentNumbers { get; set; }
        public virtual DbSet<DocumentNumberPattern> DocumentNumberPatterns { get; set; }
        public virtual DbSet<DocumentNumberLog> DocumentNumberLogs { get; set; }
        public virtual DbSet<Workflow> Workflows { get; set; }
        public virtual DbSet<TProcessRequest> TProcessRequests { get; set; }
        public virtual DbSet<TProcessActivity> TProcessActivities { get; set; }
        public virtual DbSet<TProcessActivityActor> TProcessActivityActors { get; set; }
        public virtual DbSet<ProcessActivity> ProcessActivities { get; set; }
        public virtual DbSet<ProcessActivityActor> ProcessActivityActors { get; set; }
        public virtual DbSet<ProcessRequest> ProcessRequests { get; set; }
        public virtual DbSet<RequestInbox> RequestInboxes { get; set; }
        public virtual DbSet<RequestStatus> RequestStatuses { get; set; }
        public virtual DbSet<RequestActivity> RequestActivities { get; set; }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseNpgsql("Host=localhost;Database=kflow;Username=postgres;Password=getdown");
            }
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DocumentNumber>(entity => { entity.HasIndex(e => e.DocumentCode).IsUnique(); });
            modelBuilder.Entity<Workflow>(entity => { entity.HasIndex(e => e.WorkflowCode).IsUnique(); });
            modelBuilder.Entity<TProcessRequest>(entity => { entity.HasIndex(e => new { e.CompanyId, e.ModuleId}).IsUnique(); });
        }
    }
}
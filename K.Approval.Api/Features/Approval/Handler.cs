using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using K.Approval.Api.Common;
using K.Approval.Api.DataContext;
using KCore.Common.Fault;
using KCore.Common.Response;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.EntityFrameworkCore;

namespace K.Approval.Api.Features.Approval
{
    public class Handler : IRequestHandler<Request, ApiResult<Response>>
    {
        private readonly WorkflowDataContext _workflowDataContext;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public event EventHandler<ProcessEventArgs> StatusChangedEvent;
        
        public Handler(IHttpContextAccessor httpContextAccessor, WorkflowDataContext workflowDataContext)
        {
            _workflowDataContext = workflowDataContext;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<ApiResult<Response>> Handle(Request request, CancellationToken cancellationToken)
        {
            if (request.Action != RequestAction.Approve.ToString().ToLower())
                throw new ApiException(ProcessError.InvalidRequestAction);
            var requestInbox = await 
                _workflowDataContext.RequestInboxes.FirstOrDefaultAsync(c => c.RowStatus == 0 && c.Id == request.Id, cancellationToken);
            if (requestInbox == null)  throw new ApiException(ProcessError.InvalidApprovalInbox);
            requestInbox.IsComplete = true;
            requestInbox.CompleteDate = DateTime.Now;
            requestInbox.ModifiedBy = request.GetAccessor();
            requestInbox.ModifiedDate = DateTime.UtcNow;
            requestInbox.HasView = true;
            requestInbox.ViewDate = DateTime.Now;
            requestInbox.ViewNetworkInfo = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
                
            request.RequestNumber = requestInbox.RequestNumber;
            request.ObjectId = requestInbox.ObjectId;

            var processActivity = await  _workflowDataContext.ProcessActivities.FirstOrDefaultAsync(a => a.RowStatus == 0 && a.Id == requestInbox.ProcessActivityId, cancellationToken);
            if (processActivity == null) throw new ApiException(ProcessError.InvalidProcessActivity);

            var requestStatus = await _workflowDataContext.RequestStatuses.FirstOrDefaultAsync(c => c.RowStatus == 0 && c.ProcessActivityId == requestInbox.ProcessActivityId && c.RequestNumber == requestInbox.RequestNumber, cancellationToken);
            if (requestStatus == null) throw new ApiException(ProcessError.InvalidRequestStatus);

            if (requestStatus.IsComplete != 0)
                throw new ApiException(ProcessError.DocumentAlreadyProcessedByAnotherUser(requestStatus.ActorName, requestStatus.DocumentNumber, requestStatus.DisplayStatus));

            // CHECK IS USER AUTHORIZATION
            var activityActor =
                _workflowDataContext.ProcessActivityActors.FirstOrDefault(
                    c => c.RowStatus == 0 &&
                         c.ProcessActivityId == requestInbox.ProcessActivityId && c.ActionType == (int)InboxActionType.Approval &&
                         c.ActorCode == request.UserProfile.EmployeeCode);
            if (activityActor == null)
                throw new ApiException(ProcessError.UnauthorizedApproval);


            // CHECK REQUEST ACTIVITY AND SET AS A COMPLETE REQUEST

            var requestActivities = await 
                _workflowDataContext.RequestActivities
                    .Where(
                        c => c.RowStatus == 0 &&
                             c.ProcessActivityId == requestInbox.ProcessActivityId &&
                             c.RequestNumber == requestInbox.RequestNumber).ToListAsync(cancellationToken);

            foreach (var activity in requestActivities)
            {
                activity.IsComplete = 1;
                activity.ModifiedBy = request.GetAccessor();
                activity.ModifiedDate = DateTime.UtcNow;
            }

            var requestActivity = requestActivities.FirstOrDefault(c=>c.ActorCode == request.UserProfile.EmployeeCode);
            if (requestActivity == null) throw new ApiException(ProcessError.UnauthorizedApproval);
            requestActivity.IsComplete = 1;
            requestActivity.ActionDate = requestInbox.CompleteDate;
            requestActivity.ActionName = request.Action;
            requestActivity.ActorCode = request.UserProfile.EmployeeCode;
            requestActivity.ActorName = request.UserProfile.FullName;
            requestActivity.ModifiedBy = request.GetAccessor();
            requestActivity.ModifiedDate = DateTime.UtcNow;
            requestActivity.Description = request.Description;
            requestActivity.SubjectName = ApprovalHelper.CreatePostSubject(requestActivity, requestStatus, request.UserProfile.FullName, request.Action, requestInbox.DocumentNumber);

            var requestParameter = Request.ToRequestParameter(request);
            requestParameter.DocumentNumber = requestInbox.DocumentNumber;
            requestParameter.DocumentName = requestInbox.DocumentName;
            requestParameter.RequestDate = DateTime.Now;
            ApprovalHelper approvalHelper = new ApprovalHelper(_workflowDataContext);
                
            await approvalHelper.ApprovalProcess( processActivity, requestParameter);
            await _workflowDataContext.SaveChangesAsync(cancellationToken);
            OnStatusChanged(new ProcessEventArgs
            {
                NewStatus = requestStatus.NewRequestStatus,
                DisplayStatus = approvalHelper.DisplayStatusNextActivity,
                OldStatus = processActivity.NewStatus
            });
            return ApiResult<Response>.Ok(new Response
            {
                Url = _httpContextAccessor.HttpContext?.Request?.GetDisplayUrl(),
                NewStatus = requestStatus.NewRequestStatus,
                DisplayStatus = approvalHelper.DisplayStatusNextActivity,
                RequestNumber = request.RequestNumber
            });
        }
        
        public void OnStatusChanged(ProcessEventArgs e)
        {
            var handler = StatusChangedEvent;
            handler?.Invoke(this, e);
        }
    }
}
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using K.Approval.Api.Common;
using K.Approval.Api.DataContext;
using KCore.Common.Extensions;
using KCore.Common.Fault;
using KCore.Common.Response;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.EntityFrameworkCore;

namespace K.Approval.Api.Features.Revise
{
    public class Handler : IRequestHandler<Request, ApiResult<Response>>
    {
        private readonly WorkflowDataContext _workflowDataContext;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public Handler(IHttpContextAccessor httpContextAccessor, WorkflowDataContext workflowDataContext)
        {
            _workflowDataContext = workflowDataContext;
            _httpContextAccessor = httpContextAccessor;
        }
        
        public async Task<ApiResult<Response>> Handle(Request request, CancellationToken cancellationToken)
        {
            var inbox = await
                _workflowDataContext.RequestInboxes.FirstOrDefaultAsync(
                    c => c.RowStatus == 0 && c.Id == request.InboxId, cancellationToken);
            if (inbox == null) throw new ApiException(ProcessError.InvalidApprovalInbox);
             
             inbox.IsComplete = true;
             inbox.CompleteDate = DateTime.Now;
             inbox.ModifiedBy = request.GetAccessor();
             inbox.ModifiedDate = DateTime.Now;
             inbox.HasView = true;
             inbox.ViewDate = DateTime.Now;
             inbox.ViewNetworkInfo = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
             
             request.ObjectId = inbox.ObjectId;
             request.RequestNumber = inbox.RequestNumber;
             request.DocumentName = inbox.DocumentName;
             request.DocumentNumber = inbox.DocumentNumber;

             var processActivity = await _workflowDataContext.ProcessActivities.FirstOrDefaultAsync(a =>
                 a.RowStatus == 0 && a.Id == inbox.ProcessActivityId, cancellationToken);
             if (processActivity == null) throw new ApiException(ProcessError.InvalidProcessActivity);

             var processRequest = await _workflowDataContext.ProcessRequests.FirstOrDefaultAsync(c => 
                 c.Id == processActivity.ProcessRequestId && c.RowStatus == 0, cancellationToken);
             if (processRequest == null) throw new ApiException(ProcessError.InvalidProcessActivity);


             var requestActivity = await _workflowDataContext.RequestActivities.FirstOrDefaultAsync(x =>
                 x.ProcessActivityId == inbox.ProcessActivityId && x.RequestNumber
                 == inbox.RequestNumber && x.RowStatus == 0 && x.ActorCode == request.UserProfile.EmployeeCode, cancellationToken);
             
             if (requestActivity == null) throw new ApiException(ProcessError.UnauthorizedApproval);

             
             var requestStatus =
                 await _workflowDataContext.RequestStatuses.FirstOrDefaultAsync(
                     c => c.ProcessActivityId == processActivity.Id && c.RequestNumber == inbox.RequestNumber && c.RowStatus == 0, cancellationToken);
             if (requestStatus == null) throw new ApiException(ProcessError.InvalidRequestStatus);
             if (requestStatus.IsComplete == 1)
                 throw new ApiException(ProcessError.DocumentAlreadyProcessedByAnotherUser(requestStatus.ActorName,
                     requestStatus.DocumentNumber, requestStatus.DisplayStatus));
             
             requestActivity.IsComplete = 1;
             requestActivity.ActionDate = inbox.CompleteDate;
             requestActivity.ActionName = request.ActionName;
             requestActivity.ActorCode = request.UserProfile.EmployeeCode;
             requestActivity.ActorName = request.UserProfile.FullName;
             requestActivity.ModifiedBy = request.GetAccessor();
             requestActivity.ModifiedDate = DateTime.Now;
             requestActivity.SubjectName = ApprovalHelper.CreateReviseMessage(processActivity, requestActivity, request.Action, request.UserProfile.FullName);

             var nextProcessActivity = await _workflowDataContext.ProcessActivities.FirstOrDefaultAsync(
                 c =>
                     c.RowStatus == 0 && c.ProcessRequestId == processRequest.Id &&
                     c.ActivityIndex == 1, cancellationToken);

             if (nextProcessActivity == null)
                 throw new ApiException(ProcessError.InvalidFirstIndexProcessActivity);

             var actorCreator = await
                 _workflowDataContext.ProcessActivityActors.FirstOrDefaultAsync(c =>
                     c.ProcessActivityId == nextProcessActivity.Id && c.ActorCode == requestStatus.CreatedBy, cancellationToken);
             if (actorCreator == null) throw new ApiException(ProcessError.InvalidCreatorRequestNotFound);

             requestStatus.ProcessActivityId = nextProcessActivity.Id;
             requestStatus.LastAssignDate = request.RequestDate;
             requestStatus.LastAssignTo = request.UserProfile.FullName;
             requestStatus.DisplayStatus = nextProcessActivity.DisplayName;
             requestStatus.NewRequestStatus = nextProcessActivity.NewStatus;
             requestStatus.ModifiedBy = request.GetAccessor();
             requestStatus.ModifiedDate = DateTime.Now;
             requestStatus.Notes = request.Description;

             //var reviseSubject = requestParameter.ActionName + " Approval";
             requestStatus.Subject = ApprovalHelper.CreateReviseMessage(processActivity, requestActivity, request.Action, request.UserProfile.FullName);
             requestStatus.SlaTime = nextProcessActivity.SlaTime;
             requestStatus.SlaType = nextProcessActivity.SlaType;
             requestStatus.IsComplete = 0;
             requestStatus.CompleteDate = null;
             requestStatus.ActorCode = actorCreator.ActorCode;
             requestStatus.ActorName = actorCreator.ActorName;

             var activityActors = await _workflowDataContext.ProcessActivityActors
                 .Where(c => c.ProcessActivityId == nextProcessActivity.Id).ToListAsync(cancellationToken);
             var emailTo = "";
             foreach (var activityActor in activityActors)
             {
                 if (activityActor.ActorCode != actorCreator.ActorCode)
                 {
                     emailTo += activityActor.ActorEmail + ";";
                     var requestInbox = new RequestInbox
                     {
                         Id = Guid.NewGuid(),
                         CreatedDate = DateTime.Now,
                         CreatedBy = request.GetAccessor(),
                         RowStatus = 0,
                         ModifiedBy = string.Empty,
                         ModifiedDate = DateTime.Now,
                         CommitmentDate = null,
                         RequestDate = requestStatus.RequestDate,
                         RequestNumber = request.RequestNumber,
                         DisplayStatus =
                             ApprovalHelper.SetDisplayName(processActivity, request.Action.ToRequestAction()),
                         RequestStatus =
                             ApprovalHelper.SetRequestStatus(processActivity, request.Action.ToRequestAction()),
                         ActorNameRequester = request.UserProfile.FullName,
                         Subject = ApprovalHelper.CreateReviseMessage(processActivity, requestActivity, request.Action,
                             request.UserProfile.FullName),
                         ActivityUrl = "",
                         AssignDate = DateTime.Now,
                         CompleteDate = null,
                         HasView = false,
                         IsComplete = false,
                         IsDelegatee = false,
                         RequestDelegateFromId = string.Empty,
                         RequestDelegateFromName = string.Empty,
                         ActorCodeAssignee = activityActor.ActorCode,
                         ActorNameAssignee = activityActor.ActorName,
                         ActorCodeRequester = request.UserProfile.EmployeeCode,
                         JavascriptAction = string.Empty,
                         UrlAction = string.Empty,
                         UrlActionType = 0,
                         ProcessActivityId = activityActor.ProcessActivityId,
                         Description = request.Description,
                         ObjectId = request.ObjectId,
                         ActionType = activityActor.ActionType == 2
                             ? InboxActionType.Approval.ToString()
                             : InboxActionType.View.ToString(),
                         DocumentName = request.DocumentName,
                         DocumentNumber = request.DocumentNumber
                     };
                     await _workflowDataContext.RequestInboxes.AddAsync(requestInbox, cancellationToken);
                 }
                 else
                 {
                     emailTo += activityActor.ActorEmail + ";";
                     var requestInbox = new RequestInbox
                     {
                         Id = Guid.NewGuid(),
                         CreatedDate = DateTime.Now,
                         CreatedBy = request.GetAccessor(),
                         RowStatus = 0,
                         ModifiedBy = string.Empty,
                         ModifiedDate = DateTime.Now,
                         CommitmentDate = null,
                         RequestDate = requestStatus.RequestDate,
                         RequestNumber = request.RequestNumber,
                         DisplayStatus =
                             ApprovalHelper.SetDisplayName(processActivity, request.Action.ToRequestAction()),
                         RequestStatus =
                             ApprovalHelper.SetRequestStatus(processActivity, request.Action.ToRequestAction()),
                         ActorNameRequester = request.UserProfile.FullName,
                         Subject = ApprovalHelper.CreateReviseMessage(processActivity, requestActivity, request.Action,
                             request.UserProfile.FullName),
                         ActivityUrl = "",
                         AssignDate = DateTime.Now,
                         CompleteDate = null,
                         HasView = false,
                         IsComplete = false,
                         IsDelegatee = false,
                         RequestDelegateFromId = string.Empty,
                         RequestDelegateFromName = string.Empty,
                         ActorCodeAssignee = activityActor.ActorCode,
                         ActorNameAssignee = activityActor.ActorName,
                         ActorCodeRequester = request.UserProfile.EmployeeCode,
                         JavascriptAction = string.Empty,
                         UrlAction = string.Empty,
                         UrlActionType = 0,
                         ProcessActivityId = activityActor.ProcessActivityId,
                         Description = request.Description,
                         ObjectId = request.ObjectId,
                         ActionType = InboxActionType.View.ToString(),
                         DocumentName = request.DocumentName,
                         DocumentNumber = request.DocumentNumber
                     };
                     await _workflowDataContext.RequestInboxes.AddAsync(requestInbox, cancellationToken);
                     var entity = new RequestActivity
                     {
                         Description = request.Description,
                         RequestStatus = request.Action,
                         ProcessActivityId = activityActor.ProcessActivityId,
                         ActivityIndex = nextProcessActivity.ActivityIndex,
                         DisplayStatus = request.ActionName,
                         ActorCode = actorCreator.ActorCode,
                         ActionDate = null,
                         ActorName = actorCreator.ActorName,
                         ActionName = null,
                         CreatedBy = request.GetAccessor(),
                         CreatedDate = DateTime.Now,
                         Id = Guid.NewGuid(),
                         IsComplete = 0,
                         ModifiedBy = string.Empty,
                         ModifiedDate = DateTime.Now,
                         RowStatus = 0,
                         SlaTime = null,
                         SlaType = null,
                         SubjectName = ApprovalHelper.CreateReviseMessage(processActivity, requestActivity,
                             request.Action, request.UserProfile.FullName),
                         ObjectKey = request.ObjectId,
                         RequestNumber = request.RequestNumber,
                         DocumentNumber = request.DocumentNumber,
                         DocumentName = request.DocumentName
                     };
                     await _workflowDataContext.RequestActivities.AddAsync(entity, cancellationToken);
                 }
             }

             var emailTask = ApprovalHelper.CreateEmailTask(processActivity, emailTo, request.UserProfile.EmployeeCode);

             await _workflowDataContext.EmailTasks.AddAsync(emailTask, cancellationToken);

             await _workflowDataContext.SaveChangesAsync(cancellationToken);
             /*OnStatusChanged(new ProcessEventArgs
             {
                 NewStatus = requestStatus.NewRequestStatus,
                 OldStatus = requestParameter.RequestStatus
             });*/
             return ApiResult<Response>.Ok(new Response()
             {
                 Url = _httpContextAccessor.HttpContext?.Request?.GetDisplayUrl(),
                 NewStatus = requestStatus.NewRequestStatus,
                 OldStatus = inbox.RequestStatus
             });
        }
    }
}
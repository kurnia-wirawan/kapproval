using System;
using System.Collections;
using System.Collections.Generic;
using K.Approval.Api.Common;

namespace K.Approval.Api.Features.GetInbox
{
    public class Response : BaseResponse
    {
        public IEnumerable<Result> Data { get; set; }
        public int Count { get; set; }
    }

    public class Result
    {
        public Guid Id { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
        
        public byte RowStatus { get; set; }
        public byte[] RowVersion { get; set; }
        public string ObjectId { get; set; }
        public string RequestNumber { get; set; }
        public string DocumentName { get; set; }
        public string DocumentNumber { get; set; }
        public string ActorCodeRequester { get; set; }
        public string ActorNameRequester { get; set; }
        public DateTime AssignDate { get; set; }
        public DateTime RequestDate { get; set; }
        public DateTime? CommitmentDate { get; set; }
        public string ActorCodeAssignee { get; set; }
        public string ActorNameAssignee { get; set; }
        public bool IsComplete { get; set; }
        public string ActivityUrl { get; set; }
        public string RequestStatus { get; set; }
        public string DisplayStatus { get; set; }
        public DateTime? CompleteDate { get; set; }
        public bool IsDelegatee { get; set; }
        public string RequestDelegateFromId { get; set; }
        public string RequestDelegateFromName { get; set; }
        public bool HasView { get; set; }
        public DateTime? ViewDate { get; set; }
        public string ViewNetworkInfo { get; set; }
        public byte UrlActionType { get; set; }
        public string UrlAction { get; set; }
        public string JavascriptAction { get; set; }
        public string ActionType { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
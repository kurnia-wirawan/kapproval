using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;
using K.Approval.Api.DataContext;
using KCore.Common.Data;
using KCore.Common.Response;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.EntityFrameworkCore;
using Omu.ValueInjecter;

namespace K.Approval.Api.Features.GetInbox
{
    public class Handler: IRequestHandler<Request, ApiResult<Response>>
    {
        private readonly WorkflowDataContext _workflowDataContext;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public Handler(IHttpContextAccessor httpContextAccessor, WorkflowDataContext workflowDataContext)
        {
            _workflowDataContext = workflowDataContext;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<ApiResult<Response>> Handle(Request request, CancellationToken cancellationToken)
        {
            var paging = request.Paging ?? Paging.Default;
            var sort = request.Sorts == null ? Sort.Default : !request.Sorts.Any() ? Sort.Default : request.Sorts;
            var param = new ListParameter(WhereTerm.Default(request.UserProfile.EmployeeCode, "ActorCodeAssignee"));
            param.AddRange(request.ListParameters == null ? WhereTerm.DefaultListParameters :
                !request.ListParameters.Any() ? WhereTerm.DefaultListParameters : request.ListParameters);
            
            var linqDynamicHelper = new LinqDynamicHelper();

            var queryable = _workflowDataContext.RequestInboxes.Where(linqDynamicHelper.GetQueryParameterLinq(param.ToArray()), linqDynamicHelper.ListValue.ToArray())
                .AsQueryable();
            var count = queryable.Count();

            var result = await queryable
                .OrderBy(linqDynamicHelper.OrderBy(sort))
                .Skip(paging.Start).Take(paging.Limit)
                .ToListAsync(cancellationToken);
            
            var results = new List<Result>();
            foreach (var inbox in result)
            {
                var g = new Result();
                g.InjectFrom(inbox);
                results.Add(g);
            }
            
            return ApiResult<Response>.Ok(new Response
            {
                Url = _httpContextAccessor.HttpContext?.Request?.GetDisplayUrl(),
                Data = results,
                Count = count
            });
        }
    }
}